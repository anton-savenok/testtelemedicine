package testtelemed.com.testtelemed

import android.app.Application
import ru.medlinesoft.telemed.TelemedApplicationManager


class App: Application() {

    override fun onCreate() {
        super.onCreate()

        val server = "telemed.medlinesoft.ru"
        val port = "8443"

        TelemedApplicationManager.init(server, port, this)
    }

}