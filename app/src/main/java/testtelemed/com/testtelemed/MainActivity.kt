package testtelemed.com.testtelemed

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import ru.medlinesoft.telemed.client.RequestListener
import ru.medlinesoft.telemed.client.TelemedClientManager
import ru.medlinesoft.telemed.client.rest.model.auth.AuthResult

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<View>(R.id.login_button).setOnClickListener {
            TelemedClientManager.login(
                "zzzz@zz.zz",
                "1234",
                it.context.applicationContext,
                object : RequestListener<AuthResult> {
                    override fun onFailure(p0: String?, p1: Throwable?) {
                    }

                    override fun onResponse(p0: AuthResult?) {
                    }
                }
            )
        }
    }
}
